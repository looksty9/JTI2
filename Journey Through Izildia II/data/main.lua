function math.mkp(num) if num > 0 then return num else return 0 end end

-- Print contents of `tbl`, with indentation.
-- `indent` sets the initial level of indentation.
function tprint (tbl, indent)
  if not indent then indent = 0 end
  for k, v in pairs(tbl) do
    formatting = string.rep("  ", indent) .. k .. ": "
    if type(v) == "table" then
      print(formatting)
      tprint(v, indent+1)
    elseif type(v) == 'boolean' then
      print(formatting .. tostring(v))
    elseif type(v) == 'function' then
      print(formatting .. "function")
    else
      print(formatting .. v)
    end
  end
end

function table:shallowcopy()
    local orig = self
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in pairs(orig) do
            copy[orig_key] = orig_value
        end
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end

function love.load()
  math.randomseed( os.time() )
  
  love.filesystem.setIdentity("JTI2", true)
  language = "english"
  love_default_font = love.graphics.newFont()
  
  loveframes = require("core.loveframes")
  loveframes.util.SetActiveSkin("Hot")
  
  Gamestate = require("core.hump.gamestate")
  Camera = require("core.hump.camera")
  Timer = require("core.hump.timer")
  Signal = require("core.hump.signal")
  scene = require("script.scene")
  quest = require("quest")
  control = require("script.control")
  battle = require("script.battle")
  book = require("script.book")
  
  Lady = require("core.lady")
  
  systemOS = love.system.getOS()
  window_width = 960
  window_height = 540
  window_flags = {}
  window_flags.resizable = true
  window_flags.minwidth = window_width/2
  window_flags.minheight = window_height/2
  window_flags.fullscreen = false
  window_flags.vsync = true
  window_scale = 1
  love.window.setTitle("Journey Through Izildia II: Wartime")
  love.window.setIcon(love.graphics.newImage("gui/icon.jpg"):getData())
  
  if systemOS ~= "Android" then
    local cursor = love.mouse.newCursor("gui/sprite/cursor.png", 0, 0)
    love.mouse.setCursor(cursor)
  end
  
  debug_text = ""
  skip_mainmenu = false
  
  -- Construct Objects
  music = {
    source = love.audio.newSource("sound/music/Kuroinu_Kedakaki.ogg"),
    volume = 1,
    isFading = false,
    path = "sound/music/Kuroinu_Kedakaki.ogg"
  }
  music.source:setLooping(true)

  sui = {}
  sui.click = love.audio.newSource("sound/click.wav", "static")
  sui.select = love.audio.newSource("sound/select.wav", "static")
  
  byte_image = love.graphics.newImage("gui/byte.jpg")
  
  -- Load configuration file
  config = {}
  config.language = language
  config.fontsize = 1
  if love.filesystem.isFile("config.txt") then
    window_flags, config = Lady.load_all("config.txt")
  else
    Lady.save_all("config.txt", window_flags, config)
  end
  language = config.language or "english"
  fontsize = config.fontsize or 1
  
  if systemOS ~= "Android" then
    love.window.setMode(window_width, window_height, window_flags)
  else
    love.window.setMode(0,0, {fullscreen=true, vsync=true})
    window_width, window_height = love.graphics.getDimensions()
    window_scale = window_width/960
    scene.camera:zoom(window_scale) 
  end
  
  -- Set the game state
  Gamestate.registerEvents()
  Gamestate.switch(scene.list.mainmenu)

  -- Load quest data
  quest.data = Lady.load_all("/lang/" .. language .. "/quest.txt")
end

function love.wheelmoved(x, y)
  Gamestate.current():wheelmoved(x, y)
end

function love.resize(w, h)
  if systemOS == "Android" then return end
  local s = 960/540
  if window_width ~= w then h = w/s
  elseif window_height ~= h then w = h*s end
  window_width = w
  window_height = h
  window_flags.fullscreen = love.window.getFullscreen()
  if window_flags.fullscreen == false then
    love.window.setMode(window_width, window_height, window_flags)
  end
  window_scale = window_width/960
  scene.camera:zoomTo(window_scale)
end

function love.update(dt)
    if music.source then
      music.source:setVolume(music.volume)
    end
    
    scene.camera:attach()
    loveframes.update(dt)
    scene.camera:detach()
    Timer.update(dt)
    if Effect then Effect.Timer:update(dt) end
end

function love.mousepressed(x, y, button)
    loveframes.mousepressed(x, y, button)
end

function love.mousereleased(x, y, button)
  book.mousereleased(x, y, button)
  loveframes.mousereleased(x, y, button)
end

function love.keypressed(key, unicode)
  if key == "escape" then displayExitMenu() end
  loveframes.keypressed(key, unicode)
end

function love.keyreleased(key)
  loveframes.keyreleased(key)
end

function love.textinput(text)
  loveframes.textinput(text)
end

local old = {
  getX = love.mouse.getX,
  getY = love.mouse.getY
}

function love.mouse.getX(raw)
  return old.getX() / window_scale
end

function love.mouse.getY(raw)
  return old.getY() / window_scale
end

function love.mouse.getPosition(raw)
  return love.mouse.getX(raw), love.mouse.getY(raw)
end

function string:split( inSplitPattern, outResults )
  if not outResults then
    outResults = { }
  end
  local theStart = 1
  local theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )
  while theSplitStart do
    table.insert( outResults, string.sub( self, theStart, theSplitStart-1 ) )
    theStart = theSplitEnd + 1
    theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )
  end
  table.insert( outResults, string.sub( self, theStart ) )
  return outResults
end

function key_in(key, tab)
  for v, k in pairs(tab) do
    if (type(key) == "table") then
      local is_equal = true
      for i=1,#key do
        if key[i] ~= k[i] then
          is_equal=false
          break
        end
      end  
      if is_equal then return true end
    else  
      if key == k then return true end
    end
  end
  return false
end

exitMenu = nil
function displayExitMenu()
  if exitMenu ~= nil then return end

  exitMenu = loveframes.Create("grid")
  exitMenu:SetPos(300, 200)
  exitMenu:SetRows(2)
  exitMenu:SetColumns(1)  
  exitMenu:SetCellWidth(400)
  exitMenu:SetCellHeight(50)
  exitMenu:SetCellPadding(0)

  local text = loveframes.Create("text")
  text:SetText("Go back to real life ?")
  text:SetFont(love.graphics.newFont("gui/font/Hobo.ttf", 30))
  text:Center()
  exitMenu:AddItem(text, 1, 1)

  buttons = loveframes.Create("grid")
  buttons:SetRows(1)
  buttons:SetColumns(2)  
  buttons:SetCellWidth(200)
  buttons:SetCellHeight(50)
  buttons:SetCellPadding(0)
  buttons:SetItemAutoSize(true)
  buttons:Center()
  exitMenu:AddItem(buttons, 2, 1)

  local button_cancel = loveframes.Create("button")
  button_cancel:SetText("Cancel")
  buttons:AddItem(button_cancel, 1, 1)

  local button_quitGame = loveframes.Create("button")
  button_quitGame:SetText("Quit")
  buttons:AddItem(button_quitGame, 1, 2)
  
  -------- Events ---------
  button_cancel.OnClick = function(object, x, y)
    exitMenu:Remove()
    exitMenu = nil
  end

  button_quitGame.OnClick = function(object, x, y)
    love.event.quit()
  end
end