-- get the current require path
local path = string.sub(..., 1, string.len(...) - string.len(".skins.Hot.skin"))
local loveframes = require(path .. ".libraries.common")

local skin = {}
skin.name = "Hot"
skin.author = "zenox"
skin.version = "0.1"
skin.base = "Blue"

-- controls 
skin.controls = {}
skin.controls.button_text_font = love.graphics.newFont("gui/font/Komika.ttf", 20)
skin.controls.textinput_text_normal_color = {255, 255, 255, 255}

-- multichoicerow
skin.controls.multichoicerow_body_hover_color       = {255, 153, 0, 255}

-- slider
skin.controls.slider_bar_outline_color              = {220, 220, 220, 255}

-- checkbox
skin.controls.checkbox_check_color                  = {255, 153, 0, 255}

-- radiobutton
skin.controls.radiobutton_body_color                = {255, 255, 255, 255}
skin.controls.radiobutton_check_color               = {255, 153, 0, 255}
skin.controls.radiobutton_inner_border_color        = {204, 122, 0, 255}
skin.controls.radiobutton_text_font                 = smallfont


-- columnlistrow
skin.controls.columnlistrow_body_selected_color     = {255, 153, 0, 255}
skin.controls.columnlistrow_body_hover_color        = {255, 173, 51, 255}

-- menuoption
skin.controls.menuoption_body_hover_color           = {255, 153, 0, 255}


loveframes.skins.Register(skin)