local Map = newScene()

roads = {
  ["huts"] = "river|assembly_hall",
  ["assembly_hall"] = "huts|training_camp",
  ["river"] = "huts|training_camp",
  ["forest_edge"] = "training_camp|road_to_izildia",
  ["road_to_izildia"] = "forest_edge",
  ["training_camp"] = "assembly_hall|river",
}

Map.roads = roads

function Map:init()
  if not player then player = require("script.player") end
  self.background.image = love.graphics.newImage("image/map/elf_forest.jpg")
  self.titlefont = love.graphics.newFont("gui/font/Hobo.ttf", 30)
  self.textfont = love.graphics.newFont("gui/font/Hobo.ttf", 20)
  self.title = "Elf forest"
  self.location = {}
  self.lines = {} --Precalculated lines for roads
  self.arrows = {} --Precalculated arrows for roads
  self.road_arrow = love.graphics.newImage("gui/sprite/road_arrow.png")
  self.pressed_image_button = love.graphics.newImage("gui/sprite/location_button_selected.png")
end

function Map:enter(from)
  Map.origin_x = 50
  Map.origin_y = 100
  Map:createLocation("river.png", 160, 0)
  Map:createLocation("assembly_hall.png", 160, 240)
  Map:createLocation("huts.png", 0, 120)
  Map:createLocation("forest_edge.png", 560, 240)
  Map:createLocation("road_to_izildia.png", 620, 60)
  Map:createLocation("training_camp.png", 360, 240)
  Map:calculateRoads()
  player:init()
end

function Map:draw()
  scene.camera:attach()
  love.graphics.setColor(255, 255, 255, 100)
  love.graphics.draw(self.background.image, 0, 0, 0, 0.5, 0.5)
  love.graphics.setColor(255, 255, 255, 255)
  
  Map:drawRoads()
  for _, button in pairs(self.location) do
    local width = button:GetWidth()
    local height = button:GetHeight()
    local image = button.background_image
    local imgx, imgy = image:getDimensions()
    local imgxs, imgys = width/imgx, height/imgy
    
    love.graphics.draw(button.background_image, button:GetX(), button:GetY(), 0, imgxs, imgys)
  end
  loveframes.draw()
  
  love.graphics.setFont(self.titlefont)
  love.graphics.print(self.title, 10, 10)
  
  love.graphics.setFont(self.textfont)
  love.graphics.print("Day", 740, 10)
  
  love.graphics.draw(player.image, player.posx, player.posy, 0, 0.5, 0.5)
  
  -- Draw the balck fade
  love.graphics.setColor(unpack(scene.color))
  love.graphics.draw(byte_image, 0, 0, 0, 960, 540)
  love.graphics.setColor(255, 255, 255, 255)
  
  scene.camera:detach()
end

function Map:leave()
  loveframes.util.RemoveAll()
end

function Map:createLocation(image, posx, posy)
  local imagebutton = loveframes.Create("imagebutton", frame)
  imagebutton:SetImage("gui/sprite/location_button.png")
  imagebutton:SetImageHover("gui/sprite/location_button_selected.png")
  imagebutton:SetPos(posx+self.origin_x, posy+self.origin_y)
  imagebutton:SetSize(100, 100)
  imagebutton:SetText("")
  imagebutton.overlap = true
  imagebutton.background_image = love.graphics.newImage("image/map/location/" .. image)
  imagebutton.visible = false
  
  local locname = image:sub(1, -5)
  imagebutton.OnClick = function(object, x, y) player:moveTo(locname, 3) end
  
  self.location[locname] = imagebutton
end

function Map:calculateRoads()
  local wh = 100/2
  self.lines = {}
  self.arrows = {}
  
  local this_no = {}
  for k, loc in pairs(self.location) do
    local road = roads[k]
    if road and road ~= "" then
      local dests = road:split('|')
      for _, dest in pairs(dests) do
        local line = {
          loc:GetX()+wh,
          loc:GetY()+wh,
          self.location[dest]:GetX()+wh,
          self.location[dest]:GetY()+wh
        }
        local rline = {line[3], line[4], line[1], line[2]}
        if not key_in(rline, self.lines) then
          table.insert(self.lines, line)
        else
          table.insert(this_no, rline)
        end
      end
    end
  end
  
  local sx, sy = 0.5, 0.5
  local w = 50 * sx
  local h = math.sqrt(w*w/4)
  for k, line in pairs(self.lines) do
    if not key_in(line, this_no) then
      local dx, dy = line[3]-line[1], line[4]-line[2] -- Distance, use to check arrow direction. (- Negative)
      local cx, cy = line[1]+(dx)/2, line[2]+(dy)/2   -- Center position (origin on up-left)
      local a = math.pi/4 + math.atan2(dy,dx)       -- Angle
      local fx = cx - (h*(math.cos(a) - math.sin(a)))
      local fy = cy - (h*(math.cos(a) + math.sin(a)))
      table.insert(self.arrows, {fx, fy, a, sx, sy})
    end
  end  
end

function Map:drawRoads()
  love.graphics.setColor(255, 50, 150, 255)
  love.graphics.setLineWidth(4)
  
  for _, line in pairs(self.lines) do
    love.graphics.line(unpack(line))
  end
  
  for k, arrow in pairs(self.arrows) do
    love.graphics.draw(self.road_arrow, unpack(arrow))
  end
  
  love.graphics.setColor(255, 255, 255, 255)
end

return Map